import unittest
import sys, os,json
from patchrestclient.restClient import PatchRestClient
from patch import create_app
from config.apiplus import api
from routes import route
from flask import Flask,jsonify, make_response,Blueprint
from config import settings

sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))

headers = {'Content-Type': 'application/json',
           'Accept': 'application/json'}

payload = {
  "OperationStyle": "auto",
  "TargetType": ["domu"],
  "PluginTypes": "none",
  "XmlOeda": "log/patch/37c16efa-53b3-11ea-9495-fa163e604d05/exadata_patching_oedaxml_slcs27.xml",
  "Fedramp": "DISABLED",
  "TargetEnv": "production",
  "DBPatchFile":"/scratch/jyotdas/ecra_installs/ecraMKInstall/mw_home/user_projects/domains/exacloud/PatchPayloads/19.2.9.0.0.191202/DBPatchFile/dbserver.patch.zip",
  "DomuYumRepository":"/scratch/jyotdas/ecra_installs/ecraMKInstall/mw_home/user_projects/domains/exacloud/PatchPayloads/19.2.9.0.0.191202/DomuYumRepository/exadata_ol7_19.2.9.0.0.191202_Linux-x86-64.zip",
  "ClusterID": "4",
  "BackupMode": "yes",
  "AdditionalOptions": [
    {
      "AllowActiveNfsMounts": "no",
      "RunPlugins": "no",
      "SingleUpgradeNodeName": "none",
      "EnvType": "ecs",
      "isSingleNodeUpgrade": "no",
      "IgnoreAlerts": "no",
      "RackSwitchesOnly": "no",
      "ModifyAtPrereq": "no",
      "ForceRemoveCustomRpms": "no"
    }
  ],
  "rack": {
    "backup_disk": "true"
  },
  "PayloadType": "exadata_release",
  "Retry": "yes",
  "RequestId": "domu_rollback_1_ee75-4982-b002-209c1f52b6f9",
  "EnablePlugins": "no",
  "TargetVersion": "19.2.9.0.0.191202",
  "Operation": "rollback"
}

class PatchRestClientTestCase(unittest.TestCase):

    patch_rest_client = None

    def setUp(self):
            self.app = create_app()
            self.client = self.app.test_client()

            self.patch_rest_client = PatchRestClient(aMethod='POST',
                                                 aPort = '8888',
                                                 aURI = "dom0/patch/",
                                                 aPayload = payload,
                                                 aHeaders = headers)


    def test_dom0_patch(self):
        response = self.patch_rest_client.execute()
        print response
        assert response['about'] == 'Handle DOM0 Post request'


    def test_dom0_patch_app_post(self):
        rv = self.client.post('/urest/v1/dom0/patch/', data=json.dumps(payload),content_type='application/json')
        json_data = rv.get_json()
        print(json.dumps(json_data, sort_keys=True, indent=4))
        print json_data["about"]
        assert json_data["about"] == 'Handle DOM0 Post request'

    def test_dom0_patch_app_get(self):
        rv = self.client.get('/urest/v1/dom0/patch/')
        json_data = rv.get_json()
        print(json.dumps(json_data, sort_keys=True, indent=4))
        #print json_data["about"]
        assert json_data["about"] == 'Handle Get for Dom0 Patch Request'
        

