import requests
import json
from flask import jsonify


class PatchRestClient(object):

    #headers = {'Authorization': 'Basic aWRtVHJhbnNwb3J0VXNlcjppZG1UcmFuc3BvcnRVc2Vy',
               #'Content-Type': 'application/json',
              # 'Accept': 'application/json'}

   # payload = {"passwordCredentials": {"username": "admin", "password": "propel"},"tenantName": "PROVIDER"}

    def __init__(self,
                 aMethod,
                 aPort,
                 aURI,
                 aPayload=None,
                 aProtocol='http',
                 aServer='localhost',
                 aHeaders=None,
                 ):
        '''
        Constructor
        '''
        self.__server = aServer
        self.__method = aMethod
        self.__port = aPort
        self.__protocol = aProtocol
        self.__uri = aURI
        self.__headers = aHeaders
        self.__payload = aPayload

    def get_endPoint(self):
        return self.__protocol+'://' + self.__server + ':' + self.__port + '/urest/v1/'+self.__uri

    def execute(self):
        endpoint = self.get_endPoint()
        proxies = {
            "http": None,
            "https": None,
        }


        if self.__method == 'POST':
            Response = requests.post(endpoint, headers=self.__headers, data=json.dumps(self.__payload), verify=False,proxies=proxies)
        elif self.__method == 'GET':
            Response = requests.get(endpoint, headers=self.__headers, verify=False,proxies=proxies)

        if Response:
            response = Response.json()
            return response
            #token = response.get('token').get('id')
        else:
            print('Error ocuured while calling endpoint {}'.format(self.get_endPoint()))
            #Response.raise_for_status()
            return {Response.status_code: 'Error in Handling {} Request for URL {}'.format(self.__method,self.get_endPoint())}

