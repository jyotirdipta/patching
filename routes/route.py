import os,sys
sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))

# add routes to the API.
def add_routes(api):

    #Handle for dom0 patch
    from model.dom0Patch import ns_dom0_patch
    api.add_namespace(ns_dom0_patch)

    # Handle for domu patch
    from model.domuPatch import ns_domu_patch
    api.add_namespace(ns_domu_patch)

    #Handle for dom0 Prereq
    from model.dom0PatchPrereq import ns_dom0_patch_prereq
    api.add_namespace(ns_dom0_patch_prereq)

    #Handle domuPatchPrereq
    from model.domuPatchPrereq import ns_domu_patch_prereq
    api.add_namespace(ns_domu_patch_prereq)


    # Handle for dom0 Rollback Precheck
    from model.dom0Rollbackprereq import ns_dom0_rollback_prereq
    api.add_namespace(ns_dom0_rollback_prereq)

    # Handle for domu Rollback Precheck
    from model.domuRollbackprereq import ns_domu_rollback_prereq
    api.add_namespace(ns_domu_rollback_prereq)

    # Handle for dom0 Rollback
    from model.dom0Rollback import ns_dom0_rollback
    api.add_namespace(ns_dom0_rollback)

    # Handle for domu Rollback
    from model.domuRollback import ns_domu_rollback
    api.add_namespace(ns_domu_rollback)

    # Handle for dom0 Image Backup
    from model.dom0ImageBackup import ns_dom0_image_backup
    api.add_namespace(ns_dom0_image_backup)

    # Handle for domu Image Backup
    from model.domuImageBackup import ns_domu_image_backup
    api.add_namespace(ns_domu_image_backup)

    # Handle for dom0 PostCheck
    from model.dom0PostCheck import ns_dom0_post_check
    api.add_namespace(ns_dom0_post_check)

    # Handle for domu PostCheck
    from model.domuPostCheck import ns_domu_post_check
    api.add_namespace(ns_domu_post_check)

