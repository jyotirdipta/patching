## Infra Patching Service Details

### To run the application

Using flup or gunicorn or uWSGI behind nginx is much better, because that frees up nginx to 
simply serve content, and lets you choose how many tiny light nginx threads to run, 
independently of your choice of how many heavyweight Python threads you bring up to serve dynamic content. People seem very happy with gunicorn at the moment, but any of those three options should work fine.

https://docs.gunicorn.org/en/stable/deploy.html
Although there are many HTTP proxies available, we strongly advise that you use Nginx. If you choose another proxy server you need to make sure that it buffers slow clients 
when you use default Gunicorn workers. Without this buffering Gunicorn will be easily susceptible to denial-of-service attacks. You can use Hey to check if your proxy is behaving properly.

#### Steps to follow
1. pip install -r requirements.txt
2. To run as a simple python application python patch.py (wil start on port 5000)
```buildoutcfg
root@kmaster:/home/vagrant/patching# python patch.py
__main__
2020-03-08 06:17:33,906 - __main__ - INFO - >>>>> Starting development server at http://None/api/ <<<<<
 * Serving Flask app "patch" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: on
2020-03-08 06:17:33,919 - werkzeug - INFO -  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
2020-03-08 06:17:33,922 - werkzeug - INFO -  * Restarting with stat
__main__
2020-03-08 06:17:34,492 - __main__ - INFO - >>>>> Starting development server at http://None/api/ <<<<<
2020-03-08 06:17:34,500 - werkzeug - WARNING -  * Debugger is active!
2020-03-08 06:17:34,501 - werkzeug - INFO -  * Debugger PIN: 286-702-212
```

####To run with gunicorn
1. gunicorn should be installed from the requirements.txt
2. Create the file wsgi.py
3. Ensure that the app variable is exposed in patch.py
4. Provide a wrapper in wsgi.py as shown and put it in the same folder as patch.py
5. gunicorn --bind 0.0.0.0:5000 wsgi:app

```buildoutcfg
root@kmaster:/home/vagrant/patching# gunicorn --bind 0.0.0.0:5000 wsgi:app
[2020-03-08 06:19:14 +0000] [4068] [INFO] Starting gunicorn 19.10.0
[2020-03-08 06:19:14 +0000] [4068] [INFO] Listening at: http://0.0.0.0:5000 (4068)
[2020-03-08 06:19:14 +0000] [4068] [INFO] Using worker: sync
[2020-03-08 06:19:14 +0000] [4072] [INFO] Booting worker with pid: 4072

```
6. Please note that gunicorn library might create issues in installing in windows. So better to test on Linux

####To run with uwsgi without Nginx and no uwsgi.ini file
1. Please ensure that wsgi.py file is defined
2. uwsgi --socket 0.0.0.0:8888 --protocol=http -w wsgi:app

####To run with uwsgi without Nginx and and uwsgi.ini file
1. In this case , no  wsgi.py file is required
2. Navigate to the deploy directory in the project
3. Run the command uwsgi --ini uwsgi.ini

```buildoutcfg
patching/deploy# uwsgi --ini uwsgi.ini
```

