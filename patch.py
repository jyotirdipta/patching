from flask import Flask,jsonify, make_response,Blueprint
from config.apiplus import api
from config import settings
from routes import route
import os,logging.config

app=None

# Initialise the Flask application
def create_app():
    flask_app = Flask(__name__)
    #Not used currently
    #flask_app.config['SERVER_NAME'] = settings.FLASK_SERVER_NAME
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = settings.RESTPLUS_ERROR_404_HELP

    # Configure api and add routes
    blueprint = Blueprint('api', __name__, url_prefix='/urest/v1')
    api.init_app(blueprint)
    route.add_routes(api)
    flask_app.register_blueprint(blueprint)
    return flask_app

#Configure Logging
logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), 'logging.conf'))
logging.config.fileConfig(logging_conf_path)
log = logging.getLogger(__name__)
print __name__

#Create the flask application
app = create_app()

def main():
    log.info('>>>>> Starting development server at http://{}/api/ <<<<<'.format(app.config['SERVER_NAME']))
    # Start the Flask Server at port 5000 on local host
    app.run(host='0.0.0.0', port=5000,debug=settings.FLASK_DEBUG)
    #app.run(debug=True,host='0.0.0.0')

if __name__ == "__main__":
    main()
