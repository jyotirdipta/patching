from flask import jsonify, request,abort
from flask_restplus import Resource
from config.apiplus import api
import os,sys,logging,threading
from patch_requests.serializers import dom0_patch_request
from handlers.dom0Handler import Dom0Handler

sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))
log = logging.getLogger(__name__)

ns_dom0_patch = api.namespace('dom0/patch', description='Patch Operations related to dom0')

@ns_dom0_patch.route('/')
class Dom0Patch(Resource):

    def get(self):
        return {'about':'Handle Get for Dom0 Patch Request'}

    @api.expect(dom0_patch_request)
    def post(self):
        content = request.get_json(silent=True, force=True)
        print("OperationStyle {} TargetEnv: {}".format(content["OperationStyle"],content["DBPatchFile"]))
        log.debug("from log : OperationStyle {} TargetEnv: {}".format(content.get("OperationStyle"),content.get("DBPatchFile")))

        self.__dom0PatchHandler = Dom0Handler(content)

        th = threading.Thread(target=self.__dom0PatchHandler.patch, args=(),name="Dom0PatchRequest")
        #th.setDaemon(True)
        th.start()

        """
            if (str(order).lower() == 'failurecount:desc'):
            sorts = order.split(":")
            return self.complianceDashboard.resourceBenchmark(sorts[1])
        
        """

        return {'about': 'Handle DOM0 Post request'}

