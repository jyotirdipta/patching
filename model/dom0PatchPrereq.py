from flask import jsonify, request
from flask_restplus import Resource
from config.apiplus import api
import os,sys,threading
import logging
from handlers.dom0Handler import Dom0Handler
from patch_requests.serializers import dom0_patch_request

sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))
log = logging.getLogger(__name__)
ns_dom0_patch_prereq = api.namespace('dom0/patch_prereq', description='Prereq Operations related to dom0')

@ns_dom0_patch_prereq.route('/')
class Dom0PatchPrereq(Resource):

    def get(self):
        return {'about':'Handle Get for Dom0 Pre Req checks'}

    @api.expect(dom0_patch_request)
    def post(self):
        content = request.get_json(silent=True, force=True)
        print("OperationStyle {} TargetEnv: {}".format(content["OperationStyle"], content["DBPatchFile"]))
        log.debug("from log : OperationStyle {} TargetEnv: {}".format(content.get("OperationStyle"),
                                                                      content.get("DBPatchFile")))
        self.__dom0PatchPrereqHandler = Dom0Handler(content)

        th = threading.Thread(target=self.__dom0PatchPrereqHandler.patch_prereq, args=(),name="Dom0PatchPrereq")
        # th.setDaemon(True)
        th.start()


        return {'about': 'Handle Post DOM0 Pre Req check'}
