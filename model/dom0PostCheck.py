from flask import jsonify, request
from flask_restplus import Resource
from config.apiplus import api
import os,sys,threading
import logging
from handlers.dom0Handler import Dom0Handler
from patch_requests.serializers import dom0_patch_request

sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))
log = logging.getLogger(__name__)
ns_dom0_post_check = api.namespace('dom0/post_check', description='Post Checks related to dom0')

@ns_dom0_post_check.route('/')
class Dom0PostCheck(Resource):

    def get(self):
        return {'about':'Handle Get for Dom0 Postcheck'}

    @api.expect(dom0_patch_request)
    def post(self):
        content = request.get_json(silent=True, force=True)
        print("OperationStyle {} TargetEnv: {}".format(content["OperationStyle"], content["DBPatchFile"]))
        log.debug("from log : OperationStyle {} TargetEnv: {}".format(content.get("OperationStyle"),
                                                                      content.get("DBPatchFile")))

        self.__dom0Postcheck= Dom0Handler(content)

        th = threading.Thread(target=self.__dom0Postcheck.postcheck, args=(), name="Dom0PostCheck")
        # th.setDaemon(True)
        th.start()

        """
            if (str(order).lower() == 'failurecount:desc'):
            sorts = order.split(":")
            return self.complianceDashboard.resourceBenchmark(sorts[1])

        """

        return {'about': 'Handle Post Dom0 PostCheck'}
