from flask import jsonify, request
from flask_restplus import Resource
from config.apiplus import api
import os,sys,threading
import logging
from handlers.dom0Handler import Dom0Handler
from patch_requests.serializers import dom0_patch_request

sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))
log = logging.getLogger(__name__)
ns_dom0_rollback_prereq = api.namespace('dom0/rollback_prereq', description='Rollback PreReq Check related to dom0')

@ns_dom0_rollback_prereq.route('/')
class Dom0RollbackPrereq(Resource):

    def get(self):
        return {'about':'Handle Get for Dom0 Rollback PreReqCheck'}

    @api.expect(dom0_patch_request)
    def post(self):
        content = request.get_json(silent=True, force=True)
        print("OperationStyle {} TargetEnv: {}".format(content["OperationStyle"], content["DBPatchFile"]))
        log.debug("from log : OperationStyle {} TargetEnv: {}".format(content.get("OperationStyle"),
                                                                      content.get("DBPatchFile")))

        self.__dom0RollbackprereqHandler = Dom0Handler(content)

        th = threading.Thread(target=self.__dom0RollbackprereqHandler.rollback_prereq_check, args=(), name="Dom0RollbackPrereq")
        # th.setDaemon(True)
        th.start()

        """
            if (str(order).lower() == 'failurecount:desc'):
            sorts = order.split(":")
            return self.complianceDashboard.resourceBenchmark(sorts[1])

        """

        return {'about': 'Handle Post Dom0 Rollback Prereqcheck'}
