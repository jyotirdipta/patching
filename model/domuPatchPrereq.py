from flask import jsonify, request
from flask_restplus import Resource
from config.apiplus import api
import os,sys,threading
import logging
from handlers.domuHandler import DomUHandler
from patch_requests.serializers import domU_patch_request

sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))
log = logging.getLogger(__name__)
ns_domu_patch_prereq = api.namespace('domu/patch_prereq', description='Prereq Operations related to dom0')

@ns_domu_patch_prereq.route('/')
class DomUPatchPrereq(Resource):

    def get(self):
        return {'about':'Handle Get for DomU Pre Req checks'}

    @api.expect(domU_patch_request)
    def post(self):
        content = request.get_json(silent=True, force=True)
        print("OperationStyle {} TargetEnv: {}".format(content["OperationStyle"], content["DBPatchFile"]))
        log.debug("from log : OperationStyle {} TargetEnv: {}".format(content.get("OperationStyle"),
                                                                      content.get("DBPatchFile")))

        self.__domuPatchPrereqHandler = DomUHandler(content)
        th = threading.Thread(target=self.__domuPatchPrereqHandler.patch_prereq, args=(), name="DomUPreReqcheckRequest")
        # th.setDaemon(True)
        th.start()

        return {'about': 'Handle Post DOM0 Pre Req check'}
