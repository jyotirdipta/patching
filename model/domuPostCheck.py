from flask import jsonify, request
from flask_restplus import Resource
from config.apiplus import api
import os,sys,threading
import logging
from handlers.domuHandler import DomUHandler
from patch_requests.serializers import domU_patch_request

sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))
log = logging.getLogger(__name__)
ns_domu_post_check = api.namespace('domu/post_check', description='Post Check Oerations Related to DomU')

@ns_domu_post_check.route('/')
class DomuPostCheck(Resource):

    def get(self):
        return {'about':'Handle Get for Domu Rollback PostCheck'}

    @api.expect(domU_patch_request)
    def post(self):
        content = request.get_json(silent=True, force=True)
        print("OperationStyle {} TargetEnv: {}".format(content["OperationStyle"], content["DBPatchFile"]))
        log.debug("from log : OperationStyle {} TargetEnv: {}".format(content.get("OperationStyle"),
                                                                      content.get("DBPatchFile")))

        self.__domuPostCheckHandler = DomUHandler(content)

        th = threading.Thread(target=self.__domuPostCheckHandler.postcheck, args=(), name="DomuPostCheck")
        # th.setDaemon(True)
        th.start()

        """
            if (str(order).lower() == 'failurecount:desc'):
            sorts = order.split(":")
            return self.complianceDashboard.resourceBenchmark(sorts[1])

        """

        return {'about': 'Handle Post Domu Postcheck'}
