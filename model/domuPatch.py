from flask import jsonify, request,abort
from flask_restplus import Resource
from config.apiplus import api
import os,sys,logging,threading
from patch_requests.serializers import domU_patch_request
from handlers.domuHandler import DomUHandler

sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))
log = logging.getLogger(__name__)

ns_domu_patch = api.namespace('domu/patch', description='Patch Operations related to domu')

@ns_domu_patch.route('/')
class DomUPatch(Resource):

    def get(self):
        return {'about':'Handle Get for DomU Patch Request'}

    @api.expect(domU_patch_request)
    def post(self):
        content = request.get_json(silent=True, force=True)
        print("OperationStyle {} TargetEnv: {}".format(content["OperationStyle"],content["DBPatchFile"]))
        log.debug("from log : OperationStyle {} TargetEnv: {}".format(content.get("OperationStyle"),content.get("DBPatchFile")))

        self.__domuPatchHandler = DomUHandler(content)

        th = threading.Thread(target=self.__domuPatchHandler.patch, args=(),name="DomUPatchRequest")
        #th.setDaemon(True)
        th.start()

        """
            if (str(order).lower() == 'failurecount:desc'):
            sorts = order.split(":")
            return self.complianceDashboard.resourceBenchmark(sorts[1])
        
        """

        return {'about': 'Handle DOMU Post request'}

