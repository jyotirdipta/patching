from flask import jsonify, request
from flask_restplus import Resource
from config.apiplus import api
import os,sys,threading
import logging
from patch_requests.serializers import dom0_patch_request
from handlers.dom0Handler import Dom0Handler

sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))
log = logging.getLogger(__name__)
ns_dom0_image_backup = api.namespace('dom0/image_backup', description='Image Backup related to dom0')

@ns_dom0_image_backup.route('/')
class Dom0ImageBackup(Resource):

    def get(self):
        return {'about':'Handle Get for Dom0 Image Backup'}

    @api.expect(dom0_patch_request)
    def post(self):
        content = request.get_json(silent=True, force=True)
        print("OperationStyle {} TargetEnv: {}".format(content["OperationStyle"], content["DBPatchFile"]))
        log.debug("from log : OperationStyle {} TargetEnv: {}".format(content.get("OperationStyle"),
                                                                      content.get("DBPatchFile")))

        self.__dom0ImageBackupHandler = Dom0Handler(content)

        th = threading.Thread(target=self.__dom0ImageBackupHandler.imagebackup(), args=(), name="Dom0ImageBackUp")
        # th.setDaemon(True)
        th.start()

        """
            if (str(order).lower() == 'failurecount:desc'):
            sorts = order.split(":")
            return self.complianceDashboard.resourceBenchmark(sorts[1])

        """

        return {'about': 'Handle Post Dom0 Image Backup'}
