from flask import jsonify, request
from flask_restplus import Resource
from config.apiplus import api
import os,sys,threading
import logging
from patch_requests.serializers import domU_patch_request
from handlers.domuHandler import DomUHandler

sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))
log = logging.getLogger(__name__)
ns_domu_rollback = api.namespace('domu/rollback', description='Rollback Operations related to domu')

@ns_domu_rollback.route('/')
class DomURollback(Resource):

    def get(self):
        return {'about':'Handle Get for DomU Rollback'}

    @api.expect(domU_patch_request)
    def post(self):
        content = request.get_json(silent=True, force=True)
        print("OperationStyle {} TargetEnv: {}".format(content["OperationStyle"], content["DBPatchFile"]))
        log.debug("from log : OperationStyle {} TargetEnv: {}".format(content.get("OperationStyle"),
                                                                      content.get("DBPatchFile")))

        self.__domuRollBackHandler = DomUHandler(content)

        th = threading.Thread(target=self.__domuRollBackHandler.rollback, args=(), name="DomURollBack")
        # th.setDaemon(True)
        th.start()

        """
            if (str(order).lower() == 'failurecount:desc'):
            sorts = order.split(":")
            return self.complianceDashboard.resourceBenchmark(sorts[1])

        """

        return {'about': 'Handle Post DomU Rollback'}
