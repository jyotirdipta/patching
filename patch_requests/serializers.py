from flask_restplus import fields
from config.apiplus import api

additional_options = api.model('Additional Options', {
    'AllowActiveNfsMounts': fields.String(readOnly=True, description='The unique identifier of a blog post'),
    'RunPlugins': fields.String(required=False, description='Article title'),
    'SingleUpgradeNodeName': fields.String(required=False, description='Article title'),
    'EnvType': fields.String(required=False, description='Article content'),
    'isSingleNodeUpgrade': fields.String(required=False, description='Article content'),
    'IgnoreAlerts': fields.String(required=False, description='Plugin Types'),
    'RackSwitchesOnly': fields.String(required=False, description='Plugin Types'),
    'ModifyAtPrereq': fields.String(required=False, description='Plugin Types'),
    'ForceRemoveCustomRpms': fields.String(required=False, description='Cluster ID'),
})

rack_model = api.model('Backup Disc', {
    'backup_disk': fields.String(readOnly=True, description='Backup Disk'),
})

generic_request = api.model('Infra Patching Generic request', {
    'Operation': fields.String(required=True, description='Infra Patching Related operations'),
    'OperationStyle': fields.String(required=False, description=''),
    'TargetType': fields.List(fields.String),
    'PluginTypes': fields.String(required=False, description=''),
    'XmlOeda': fields.String(required=False, description=''),
    'Fedramp': fields.String(required=False, description=''),
    'PluginTypes': fields.String(required=False, description=''),
    'DBPatchFile': fields.String(required=False, description='Location of DB Patch file'),
    'ClusterID': fields.String(required=False, description='Cluster ID'),
    'BackupMode': fields.String(required=False, description='BackupMode'),
    'AdditionalOptions': fields.List(fields.Nested(additional_options)),
    'rack': fields.Nested(rack_model),
    'PayloadType': fields.String(required=False, description='Exadata PayloadType'),
    'Retry': fields.String(required=False),
    'RequestId': fields.String(required=False, description='Reuqest ID of the Operation'),
    'EnablePlugins': fields.String(required=False, description='Plugins to be fired or not'),
    'TargetVersion': fields.String(required=False, description='Article content'),

})

dom0_patch_request = api.inherit('Patch Request for Dom0', generic_request, {
    'Dom0YumRepository': fields.String(required=False, description=''),
})

domU_patch_request = api.inherit('Patch Request for DomU', generic_request, {
    'DomuYumRepository': fields.String(required=False, description=''),
})

