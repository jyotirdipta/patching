def from_str(x):
    assert isinstance(x, (str, unicode))
    return x


def from_stringified_bool(x):
    if x == "true":
        return True
    if x == "false":
        return False
    assert False


def from_list(f, x):
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c, x):
    assert isinstance(x, c)
    return x.to_dict()

from patch_requests.additionalOption import AdditionalOption
from patch_requests.rack import Rack

class dom0PatchRequest:
    def __init__(self, operation_style, target_type, plugin_types, xml_oeda, fedramp, target_env, db_patch_file, cluster_id, backup_mode, additional_options, rack, payload_type, retry, request_id, enable_plugins, dom0_yum_repository, target_version, operation):
        self.operation_style = operation_style
        self.target_type = target_type
        self.plugin_types = plugin_types
        self.xml_oeda = xml_oeda
        self.fedramp = fedramp
        self.target_env = target_env
        self.db_patch_file = db_patch_file
        self.cluster_id = cluster_id
        self.backup_mode = backup_mode
        self.additional_options = additional_options
        self.rack = rack
        self.payload_type = payload_type
        self.retry = retry
        self.request_id = request_id
        self.enable_plugins = enable_plugins
        self.dom0_yum_repository = dom0_yum_repository
        self.target_version = target_version
        self.operation = operation

    @staticmethod
    def from_dict(obj):
        assert isinstance(obj, dict)
        operation_style = from_str(obj.get(u"OperationStyle"))
        target_type = from_list(from_str, obj.get(u"TargetType"))
        plugin_types = from_str(obj.get(u"PluginTypes"))
        xml_oeda = from_str(obj.get(u"XmlOeda"))
        fedramp = from_str(obj.get(u"Fedramp"))
        target_env = from_str(obj.get(u"TargetEnv"))
        db_patch_file = from_str(obj.get(u"DBPatchFile"))
        cluster_id = int(from_str(obj.get(u"ClusterID")))
        backup_mode = from_str(obj.get(u"BackupMode"))
        additional_options = from_list(AdditionalOption.from_dict, obj.get(u"AdditionalOptions"))
        rack = Rack.from_dict(obj.get(u"rack"))
        payload_type = from_str(obj.get(u"PayloadType"))
        retry = from_str(obj.get(u"Retry"))
        request_id = from_str(obj.get(u"RequestId"))
        enable_plugins = from_str(obj.get(u"EnablePlugins"))
        dom0_yum_repository = from_str(obj.get(u"Dom0YumRepository"))
        target_version = from_str(obj.get(u"TargetVersion"))
        operation = from_str(obj.get(u"Operation"))
        return dom0PatchRequest(operation_style, target_type, plugin_types, xml_oeda, fedramp, target_env, db_patch_file, cluster_id, backup_mode, additional_options, rack, payload_type, retry, request_id, enable_plugins, dom0_yum_repository, target_version, operation)

    def to_dict(self):
        result = {}
        result[u"OperationStyle"] = from_str(self.operation_style)
        result[u"TargetType"] = from_list(from_str, self.target_type)
        result[u"PluginTypes"] = from_str(self.plugin_types)
        result[u"XmlOeda"] = from_str(self.xml_oeda)
        result[u"Fedramp"] = from_str(self.fedramp)
        result[u"TargetEnv"] = from_str(self.target_env)
        result[u"DBPatchFile"] = from_str(self.db_patch_file)
        result[u"ClusterID"] = from_str(str(self.cluster_id))
        result[u"BackupMode"] = from_str(self.backup_mode)
        result[u"AdditionalOptions"] = from_list(lambda x: to_class(AdditionalOption, x), self.additional_options)
        result[u"rack"] = to_class(Rack, self.rack)
        result[u"PayloadType"] = from_str(self.payload_type)
        result[u"Retry"] = from_str(self.retry)
        result[u"RequestId"] = from_str(self.request_id)
        result[u"EnablePlugins"] = from_str(self.enable_plugins)
        result[u"Dom0YumRepository"] = from_str(self.dom0_yum_repository)
        result[u"TargetVersion"] = from_str(self.target_version)
        result[u"Operation"] = from_str(self.operation)
        return result


def dom0PatchRequest_from_dict(s):
    return dom0PatchRequest.from_dict(s)


def dom0PatchRequest_to_dict(x):
    return to_class(dom0PatchRequest, x)
