def from_str(x):
    assert isinstance(x, (str, unicode))
    return x


def from_stringified_bool(x):
    if x == "true":
        return True
    if x == "false":
        return False
    assert False


def from_list(f, x):
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c, x):
    assert isinstance(x, c)
    return x.to_dict()

class AdditionalOption:
    def __init__(self, allow_active_nfs_mounts, run_plugins, single_upgrade_node_name, env_type, is_single_node_upgrade, ignore_alerts, rack_switches_only, modify_at_prereq, force_remove_custom_rpms):
        self.allow_active_nfs_mounts = allow_active_nfs_mounts
        self.run_plugins = run_plugins
        self.single_upgrade_node_name = single_upgrade_node_name
        self.env_type = env_type
        self.is_single_node_upgrade = is_single_node_upgrade
        self.ignore_alerts = ignore_alerts
        self.rack_switches_only = rack_switches_only
        self.modify_at_prereq = modify_at_prereq
        self.force_remove_custom_rpms = force_remove_custom_rpms

    @staticmethod
    def from_dict(obj):
        assert isinstance(obj, dict)
        allow_active_nfs_mounts = from_str(obj.get(u"AllowActiveNfsMounts"))
        run_plugins = from_str(obj.get(u"RunPlugins"))
        single_upgrade_node_name = from_str(obj.get(u"SingleUpgradeNodeName"))
        env_type = from_str(obj.get(u"EnvType"))
        is_single_node_upgrade = from_str(obj.get(u"isSingleNodeUpgrade"))
        ignore_alerts = from_str(obj.get(u"IgnoreAlerts"))
        rack_switches_only = from_str(obj.get(u"RackSwitchesOnly"))
        modify_at_prereq = from_str(obj.get(u"ModifyAtPrereq"))
        force_remove_custom_rpms = from_str(obj.get(u"ForceRemoveCustomRpms"))
        return AdditionalOption(allow_active_nfs_mounts, run_plugins, single_upgrade_node_name, env_type, is_single_node_upgrade, ignore_alerts, rack_switches_only, modify_at_prereq, force_remove_custom_rpms)

    def to_dict(self):
        result = {}
        result[u"AllowActiveNfsMounts"] = from_str(self.allow_active_nfs_mounts)
        result[u"RunPlugins"] = from_str(self.run_plugins)
        result[u"SingleUpgradeNodeName"] = from_str(self.single_upgrade_node_name)
        result[u"EnvType"] = from_str(self.env_type)
        result[u"isSingleNodeUpgrade"] = from_str(self.is_single_node_upgrade)
        result[u"IgnoreAlerts"] = from_str(self.ignore_alerts)
        result[u"RackSwitchesOnly"] = from_str(self.rack_switches_only)
        result[u"ModifyAtPrereq"] = from_str(self.modify_at_prereq)
        result[u"ForceRemoveCustomRpms"] = from_str(self.force_remove_custom_rpms)
        return result

