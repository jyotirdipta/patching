def from_str(x):
    assert isinstance(x, (str, unicode))
    return x


def from_stringified_bool(x):
    if x == "true":
        return True
    if x == "false":
        return False
    assert False


def from_list(f, x):
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c, x):
    assert isinstance(x, c)
    return x.to_dict()

class Rack:
    def __init__(self, backup_disk):
        self.backup_disk = backup_disk

    @staticmethod
    def from_dict(obj):
        assert isinstance(obj, dict)
        backup_disk = from_stringified_bool(from_str(obj.get(u"backup_disk")))
        return Rack(backup_disk)

    def to_dict(self):
        result = {}
        result[u"backup_disk"] = from_str(str(self.backup_disk).lower())
        return result