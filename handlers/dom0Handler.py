import os, sys,logging
from flask import abort

sys.path.append(os.path.join(os.path.dirname(__file__), os.path.pardir))
log = logging.getLogger(__name__)

class Dom0Handler():

    def __init__(self,jsoncontent):
        self.__payload = jsoncontent

    def patch_prereq(self):

        try:
            print "Patch prereq"
            print("OperationStyle {} TargetEnv: {}".format(self.__payload["OperationStyle"], self.__payload["DBPatchFile"]))
            log.debug("from log : OperationStyle {} TargetEnv: {}".format(self.__payload.get("OperationStyle"),
                                                                          self.__payload.get("DBPatchFile")))
        except:
            print "Failed Patch prereq"
            return {'data': 'Failed Patch prereq'}, 401
        finally:
            #abort(401)
            print "finally from thread"
            return {'data': 'Failed to create resource dataHandler'}, 401

    def patch(self):

        try:
            print "Patching"
            print("OperationStyle {} TargetEnv: {}".format(self.__payload["OperationStyle"], self.__payload["DBPatchFile"]))
            log.debug("from log : OperationStyle {} TargetEnv: {}".format(self.__payload.get("OperationStyle"),
                                                                          self.__payload.get("DBPatchFile")))
        except:
            print "Failed to create Patch Request"
            return {'data': 'Failed to create Patch Request'}, 401
        finally:
            #abort(401)
            print "finally from thread"
            return {'data': 'Done with Patch Request'}, 401


    def rollback(self):

        try:
            print "Rollback"
            print("OperationStyle {} TargetEnv: {}".format(self.__payload["OperationStyle"],
                                                           self.__payload["DBPatchFile"]))
            log.debug("from log : OperationStyle {} TargetEnv: {}".format(self.__payload.get("OperationStyle"),
                                                                          self.__payload.get("DBPatchFile")))
        except:
            print "Failed to create rollback"
            return {'data': 'Failed to create rollback'}, 401
        finally:
            print "finally"
            # verticaConn.close()

    def rollback_prereq_check(self):

        try:
            print "Rollback_prereq_check"
            print("OperationStyle {} TargetEnv: {}".format(self.__payload["OperationStyle"],
                                                           self.__payload["DBPatchFile"]))
            log.debug("from log : OperationStyle {} TargetEnv: {}".format(self.__payload.get("OperationStyle"),
                                                                          self.__payload.get("DBPatchFile")))
        except:
            print "Failed to RollBack Prereq check"
            return {'data': 'Failed RollBack Prereq check'}, 401
        finally:
            print "finally"
            # verticaConn.close()

    def imagebackup(self):

        try:
            print "Image Backup"
            print("OperationStyle {} TargetEnv: {}".format(self.__payload["OperationStyle"],
                                                           self.__payload["DBPatchFile"]))
            log.debug("from log : OperationStyle {} TargetEnv: {}".format(self.__payload.get("OperationStyle"),
                                                                          self.__payload.get("DBPatchFile")))
        except:
            print "Failed to create Image Backup"
            return {'data': 'Failed to create Image Backup'}, 401
        finally:
            print "finally"
            # verticaConn.close()

    def postcheck(self):

        try:
            print "Postcheck"
            print("OperationStyle {} TargetEnv: {}".format(self.__payload["OperationStyle"],
                                                           self.__payload["DBPatchFile"]))
            log.debug("from log : OperationStyle {} TargetEnv: {}".format(self.__payload.get("OperationStyle"),
                                                                          self.__payload.get("DBPatchFile")))
        except:
            print "Failed to Perform PostCheck"
            return {'data': 'Failed to create Image Backup'}, 401
        finally:
            print "finally"


